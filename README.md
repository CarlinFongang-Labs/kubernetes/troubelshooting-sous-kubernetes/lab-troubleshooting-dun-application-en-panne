# Lab : Résolution des Problèmes d'Applications Kubernetes Défectueuses

------------

><img src="https://i.pinimg.com/280x280_RS/6b/68/be/6b68bed191fdd2fad36e4193e64764ee.jpg" width="50" height="50" alt="Carlin Fongang">

>Carlin FONGANG | fongangcarlin@gmail.com

>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://github.com/carlinfongang) | [Credly](https://www.credly.com/users/carlin-fongang/badges)

_______


# Objectifs

1. Identifiez ce qui ne va pas avec l'application

2. Résoudre le problème

# Contexte

Votre entreprise, BeeHive, crée des applications pour Kubernetes. Vos développeurs ont récemment déployé une application sur votre cluster, mais celle-ci rencontre quelques problèmes.

Un ensemble de Pods gérés par le `web-consumer` déploiement adresse régulièrement des requêtes à un service qui fournit des données d'authentification. Vos développeurs signalent que les conteneurs ne se comportent pas comme prévu.

Votre tâche consiste à examiner l'application en question, à déterminer le problème et à le résoudre.

>![Alt text](img/image.png)

# Introduction

Les administrateurs Kubernetes doivent être en mesure de résoudre les problèmes liés aux applications exécutées dans un cluster. Cet atelier vous permettra de tester vos compétences en matière de réparation d'applications Kubernetes défectueuses. Un cluster exécutant une application défectueuse vous sera présenté et il vous sera demandé d'identifier et de corriger le problème.

# Application

## Étape 1 : Connexion au Serveur

Connectez-vous au serveur à l'aide des informations d'identification fournies :

```sh
ssh -i id_rsa user@<PUBLIC_IP_ADDRESS>
```

## Étape 2 : Identification du Problème avec l'Application

1. Examinez le déploiement `web-consumer` qui réside dans l'espace de noms `web` et ses pods :

```sh
kubectl get deployment -n web web-consumer
```

>![Alt text](img/image-1.png)
*Liste des deployments*

2. Obtenez plus d'informations sur le déploiement :

```sh
kubectl describe deployment -n web web-consumer
```

- Incluez les spécifications des conteneurs et les labels utilisées par les pods.

>![Alt text](img/image-2.png)
*Description du deployment web-consumer*

3. Examinez de plus près les pods pour voir s'ils sont opérationnels :

```sh
kubectl get pods -n web
```

>![Alt text](img/image-3.png)

4. Obtenez plus d'informations sur les pods :

```sh
kubectl describe pod -n web <POD_NAME>
```

- Évaluez les messages d'avertissement qui pourraient apparaître.

>![Alt text](img/image-4.png)
*Description du conteneur 1*

>![Alt text](img/image-5.png)
*Description du conteneur 2*

5. Consultez les journaux associés au conteneur `busybox` :

```sh
kubectl logs -n web web-consumer-84fc79d94d-c4l9p -c busybox
```

>![Alt text](img/image-6.png)
*Logs du conteneur*

6. Déterminez ce qui ne va pas en lisant le résultat des journaux du conteneur.

En consultant les journaux du conteneur, l'on obtiens comme message : **"curl: (6) Couldn't resolve host 'auth-db'"**, ainsi le conteneur n'arrive pas à contacter le service **"auth-db"**

7. Examinez de plus près le pod lui-même pour obtenir les données au format YAML :

```sh
kubectl get pod -n web <POD_NAME> -o yaml
```

8. Déterminez quelle commande est à l’origine des erreurs (dans ce cas, la commande `while true; do curl auth-db; sleep 5; done`).

>![Alt text](img/image-7.png)
*Identification de la commande à l'origine du problème*

## Étape 3 : Résolution du Problème

1. Examinez de plus près le service `auth-db` :

```sh
kubectl get svc -n web auth-db
```

>![Alt text](img/image-8.png)

Le service auth-db ne se trouve pas dans le même namespace que nos pods d'après ce résultat.

2. Localisez où se trouve le service en vérifiant les espaces de noms :

```sh
kubectl get namespaces
```

- Recherchez l'espace de noms autre que celui par défaut, appelé `data`.

>![Alt text](img/image-9.png)
*Liste de namespace*

3. Vérifiez l'espace de noms `data` et trouvez le service `auth-db` :

```sh
kubectl get svc -n data
```

>![Alt text](img/image-10.png)
*Liste de svc dans le ns data*

4. Résolvez le problème en éditant le déploiement `web-consumer` :

```sh
kubectl edit deployment -n web web-consumer
```

5. Dans la section `spec`, faites défiler vers le bas pour trouver le modèle de pod et localisez la commande `while true; do curl auth-db; sleep 5; done`.

6. Remplacez la commande par :

```sh
while true; do curl auth-db.data.svc.cluster.local; sleep 5; done
```

>![Alt text](img/image-11.png)
*Edition du deployment web-consumer*

- Cela permettra aux pods `web-consumer` du déploiement de communiquer correctement avec le service.

7. Enregistrez le fichier et quittez en appuyant sur la touche `ESC` et en utilisant `:wq`.

>![Alt text](img/image-12.png)
*Enregistrement du deployment edité*

8. Vérifiez que les anciens pods sont terminés et que les nouveaux pods fonctionnent correctement :

```sh
kubectl get pods -n web
```

>![Alt text](img/image-13.png)
*Les pods pods ont redemarré*

9. Vérifiez le journal de l'un des nouveaux pods :

```sh
kubectl logs -n web web-consumer-746567899c-7mwkg -c busybox
```

>![Alt text](img/image-14.png)
*Service Nginx opérationel*

- Cette fois, le pod devrait pouvoir communiquer avec succès avec le service.

Ce laboratoire vous guide à travers le processus d'identification et de résolution des problèmes d'applications Kubernetes défectueuses en examinant les déploiements, les pods, les services et en modifiant les configurations pour corriger les erreurs.